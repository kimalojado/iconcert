const nav = document.querySelector("#navbar")

if(window.scrollY >= 10) {
	nav.classList.add("bg-primary")
}

window.onscroll = () => {
	if(this.scrollY <=10) {
		nav.classList.remove("bg-primary")
		nav.classList.add("bg-transparent")
	} else {
		nav.classList.remove("bg-transparent")
		nav.classList.add("bg-primary")
	}
}





// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }



